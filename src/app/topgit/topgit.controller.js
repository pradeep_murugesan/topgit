(function() {
  'use strict';

  angular
    .module('topgitGulp')
    .controller('TopGitController', TopGitController);

  /** @ngInject */
  function TopGitController($log, github, linkHeaderParser) {
    var vm = this;
    vm.slider = {
      value: 500,
      options: {
        floor: 0,
        ceil: 1000
      }
    };
    vm.showSearchOptions = false;
    vm.searchLanguage = "php";
    vm.currentPage = 1;
    vm.showLoader = false;
    activate();

    function activate() {
      getRepos();
    }

    function getRepos() {
      if(vm.currentPage == 1 || vm.currentPage < vm.lastPage) {
        vm.showLoader = true;
        $log.info("Getting the data for page" + vm.currentPage);
        github.getRepos(vm.currentPage, vm.searchLanguage, vm.slider.value).then(function (data) {
          vm.showLoader = false;
          vm.repositories = data;
          var headers = vm.repositories.headers;
          vm.remaining = headers["x-ratelimit-remaining"];
          vm.total = headers["x-ratelimit-limit"];
          vm.lastPage = linkHeaderParser.getLastPage(headers['link']);
        }).finally(function (data) {
          $log.error(data);
          vm.showLoader = false;
        });
      }
    }

    vm.search = function () {
      vm.currentPage = 1;
      getRepos();
      vm.toggleSearchOptions();
    }

    vm.toggleSearchOptions = function () {
      vm.showSearchOptions = !vm.showSearchOptions;

    };

    vm.getPreviousPage = function() {
      if(vm.currentPage > 1) {
        vm.currentPage--;
      }
      getRepos();
    };

    vm.getNextPage = function() {
      if(vm.currentPage < vm.lastPage) {
        vm.currentPage++;
      }
      getRepos();
    };


  }
})();
