(function() {
  'use strict';

  angular
    .module('topgitGulp')
    .factory('linkHeaderParser', linkHeaderParser);

  /** @ngInject */
  function linkHeaderParser($log) {
    var service = {
      getNext: getNext,
      getLastPage: getLastPage
    };

    function getNext(linkHeader) {
      $log.info(linkHeader);
      var parts = linkHeader.split(",");
      var link = '';
      angular.forEach(parts, function(value) {
        if(value.indexOf('rel="next"') != -1) {
          var tokens = value.split(";");
          link = tokens[0];
          link = link.replace(">","");
          link = link.replace("<","");
        }
      });
      return link;
    }

    function getLastPage(linkHeader) {
      var parts = linkHeader.split(",");
      var pages;
      angular.forEach(parts, function(value) {
        if(value.indexOf('rel="last"') != -1) {
          var regEx = /page=(\d+)/;
          var matches = value.match(regEx);
          pages = matches[1];
        }
      });
      return pages;
    }

    return service;
  }

})();
