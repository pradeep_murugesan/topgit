(function() {
  'use strict';

  angular
    .module('topgitGulp')
    .factory('github', github);

  /** @ngInject */
  function github($log, $http) {
    var api = 'https://api.github.com/search/repositories';
    //var api = 'http://jwttester.getsandbox.com/contributors'
    var service = {
      getRepos: getRepos
    };

    return service;

    function getRepos(page, language, stars) {
      var query = "?q=stars:>=" + stars + " language:" + language + "&sort=stars&page=" + page;
      return $http.get(api + query)
        .then(getReposComplete)
        .catch(getReposFailed);

      function getReposComplete(response) {
        response.data.headers = response.headers();
        return response.data;
      }

      function getReposFailed(error) {
        $log.error('XHR Failed for get Repositories.\n' + angular.toJson(error.data, true));
      }
    }
  }
})();
