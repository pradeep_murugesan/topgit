(function() {
  'use strict';

  angular
    .module('topgitGulp')
    .directive('paginationControls', pagination);

  /** @ngInject */
  function pagination() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/directives/pagination/pagination.html',
      scope: {
        nextPage: '&',
        previousPage: '&',
        currentPage: '=',
        lastPage: '='
      }
    };

    return directive;

  }

})();
