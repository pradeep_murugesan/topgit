(function() {
  'use strict';

  angular
    .module('topgitGulp')
    .directive('repositoryDetail', repositoryDetail);

  /** @ngInject */
  function repositoryDetail() {
    var directive = {
      restrict: 'E',
      templateUrl: 'app/directives/repository-detail/repository-detail.html',
      scope: {
        repository: '='
      }
    };

    return directive;

  }

})();
