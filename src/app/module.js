(function() {
  'use strict';

  angular
    .module('topgitGulp', ['ngAnimate', 'ngCookies', 'ngResource', 'ngRoute', 'rzModule']);

})();
