(function() {
  'use strict';

  angular
    .module('topgitGulp')
    .config(routeConfig);

  function routeConfig($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'app/topgit/topgit.html',
        controller: 'TopGitController',
        controllerAs: 'topgit'
      })
      .otherwise({
        redirectTo: '/'
      });
  }

})();
